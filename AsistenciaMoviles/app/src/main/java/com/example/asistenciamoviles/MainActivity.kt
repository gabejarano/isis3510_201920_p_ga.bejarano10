package com.example.asistenciamoviles

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.bluetooth.BluetoothAdapter
import android.widget.*

import android.content.Intent
import android.R.id.button1

import android.widget.Toast
import android.R.id.button2
import android.os.AsyncTask
import android.os.StrictMode
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class MainActivity : AppCompatActivity() {

    private val REQUEST_ENABLE_BT = 0
    private val REQUEST_DISCOVERABLE_BT = 0
    private var date = LocalDateTime.now()
    private var cadenaformateador = DateTimeFormatter.ofPattern("d-M-YYYY")
    private var formateado= date.format(cadenaformateador)



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val politica = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(politica)
        setContentView(R.layout.activity_main)

        val out : TextView =findViewById(R.id.name_bt)
        val button1: Button =  findViewById(R.id.button1)
        val button2: Button =  findViewById(R.id.button2)
        val button3: Button =  findViewById(R.id.button3)
        val buttonBuscar : Button= findViewById(R.id.db)
        val mBluetoothAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()




        if (mBluetoothAdapter == null) {
            out.append("device not supported")
        }
        button1.setOnClickListener {
            if (!mBluetoothAdapter.isEnabled) {
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
            }
        }

        button2.setOnClickListener {
            if (!mBluetoothAdapter.isDiscovering) {
                //out.append("MAKING YOUR DEVICE DISCOVERABLE");
                Toast.makeText (applicationContext, "MAKING YOUR DEVICE DISCOVERABLE",
                    Toast.LENGTH_LONG
                ).show()
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
                startActivityForResult(enableBtIntent, REQUEST_DISCOVERABLE_BT)

            }
        }

        button3.setOnClickListener {
            mBluetoothAdapter.disable()
            //out.append("TURN_OFF BLUETOOTH")
            Toast.makeText(getApplicationContext(), "TURNING_OFF BLUETOOTH", Toast.LENGTH_LONG).show()

        }

        buttonBuscar.setOnClickListener{
            var mesAterior = formateado.split("-").get(1)
            var mesQuesEs = mesAterior.toInt()-1
            var stringHTTP = formateado.split("-")[0]+"-"+mesQuesEs+"-"+formateado.split("-")[2]



            try{
                URL("https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/"+stringHTTP+"/students/201613022")
                            .openStream().bufferedReader().use { it.readText() }

                Toast.makeText(this@MainActivity, "Asistió", Toast.LENGTH_SHORT).show()
            } catch (e: Exception)
            {
                Toast.makeText(this@MainActivity, "No está en la Base de datos", Toast.LENGTH_SHORT).show()
            }



        }



    }





}
